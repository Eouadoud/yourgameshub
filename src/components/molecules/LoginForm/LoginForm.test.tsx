import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { LoginForm } from "./LoginForm";
const mockRegisterFunction = jest.fn();

describe("LoginForm Component", () => {
  it("test default render", () => {
    render(<LoginForm loginFunction={mockRegisterFunction} />);
    expect(screen.getByText("Nom d'utilisateur")).toBeInTheDocument();
    expect(screen.getByText("Valider")).toBeInTheDocument();
    const iconElement = screen.queryByRole("iconspinner");
    expect(iconElement).toBeNull();
  });

  it("test loading render", () => {
    render(<LoginForm loginFunction={mockRegisterFunction} isLoading={true} />);
    const firstNameInput = screen.queryByText("Prénom");
    expect(firstNameInput).not.toBeInTheDocument();
    expect(screen.getByRole("iconspinner")).toBeInTheDocument();
  });

  it("test sumbit button click", async () => {
    render(<LoginForm loginFunction={mockRegisterFunction} />);

    const validerButton = screen.getByText("Valider");
    await userEvent.click(validerButton);

    expect(mockRegisterFunction).toHaveBeenCalled();
  });
});
