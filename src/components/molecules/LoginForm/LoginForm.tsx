import { ChangeInputValueEvent, inputTypes } from "@/types/Input";
import { FC, useState } from "react";
import styles from "./LoginForm.module.scss";
import { TextInput } from "@/components/atoms/InputText/TextInput";
import { Button } from "@/components/atoms/Button/Button";
import { IconSpinner } from "@/components/atoms/IconSpinner/IconSpinner";

type loginFormPropos = {
  loginFunction?: () => void;
  userNameHasDanger?: boolean;
  userNameValiated?: boolean;
  passwordHasDanger?: boolean;
  passwordValiated?: boolean;
  isLoading?: boolean;
};

export const LoginForm: FC<loginFormPropos> = ({
  loginFunction,
  userNameHasDanger = false,
  userNameValiated = false,
  passwordHasDanger = false,
  passwordValiated = false,
  isLoading = false,
}) => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const handleChange = (
    event: ChangeInputValueEvent,
    setterFunc: (input: string) => void
  ) => {
    setterFunc(event.target.value);
  };
  return (
    <div className={styles["formInner"]}>
      {!isLoading && (
        <div>
          <TextInput
            className={"form-group m-2"}
            value={userName}
            onChange={(event) => handleChange(event, setUserName)}
            hasDanger={userNameHasDanger}
            wasValiated={userNameValiated}
            label="Nom d'utilisateur"
            type={inputTypes.text}
          />
          <TextInput
            className={"form-group m-2"}
            value={password}
            onChange={(event) => handleChange(event, setPassword)}
            hasDanger={passwordHasDanger}
            wasValiated={passwordValiated}
            label="Mot de passe"
            type={inputTypes.password}
          />
          <Button
            stylesType="danger"
            textContent="Valider"
            onClick={loginFunction}
          ></Button>
        </div>
      )}
      {isLoading && <IconSpinner textMessage="Changement..."></IconSpinner>}
    </div>
  );
};
