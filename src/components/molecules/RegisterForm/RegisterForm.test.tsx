import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { RegisterForm } from "./RegisterForm";
const mockRegisterFunction = jest.fn();

describe("RegisterForm Component", () => {
  it("test default render", () => {
    render(<RegisterForm registerFunction={mockRegisterFunction} />);
    expect(
      screen.getByText(
        "Nom d'utilisateur - Composé uniquement des lettres et des chiffres."
      )
    ).toBeInTheDocument();
    expect(screen.getByText("Prénom")).toBeInTheDocument();
    expect(screen.getByText("Nom de famille")).toBeInTheDocument();
    expect(screen.getByText("Mot de passe")).toBeInTheDocument();
    expect(screen.getByText("Valider")).toBeInTheDocument();
    const iconElement = screen.queryByRole("iconspinner");
    expect(iconElement).toBeNull();
  });

  it("test loading render", () => {
    render(
      <RegisterForm registerFunction={mockRegisterFunction} isLoading={true} />
    );
    const firstNameInput = screen.queryByText("Prénom");
    expect(firstNameInput).not.toBeInTheDocument();
    expect(screen.getByRole("iconspinner")).toBeInTheDocument();
  });

  it("test sumbit button click", async () => {
    render(<RegisterForm registerFunction={mockRegisterFunction} />);

    const validerButton = screen.getByText("Valider");
    await userEvent.click(validerButton);

    expect(mockRegisterFunction).toHaveBeenCalled();
  });
});
