import { TextInput } from "@/components/atoms/InputText/TextInput";
import { ChangeInputValueEvent, inputTypes } from "@/types/Input";
import { FC, useState } from "react";
import styles from "./RegisterForm.module.scss";
import { Button } from "@/components/atoms/Button/Button";
import { IconSpinner } from "@/components/atoms/IconSpinner/IconSpinner";

type spinnerProps = {
  registerFunction?: () => void;
  mailAdressHasDanger?: boolean;
  mailAdressValiated?: boolean;
  userNameHasDanger?: boolean;
  userNameValiated?: boolean;
  firstNameHasDanger?: boolean;
  firstNameValiated?: boolean;
  lastNameHasDanger?: boolean;
  lastNameValiated?: boolean;
  passwordHasDanger?: boolean;
  passwordValiated?: boolean;
  isLoading?: boolean;
};

export const RegisterForm: FC<spinnerProps> = ({
  registerFunction,
  mailAdressHasDanger = false,
  mailAdressValiated = false,
  userNameHasDanger = false,
  userNameValiated = false,
  firstNameHasDanger = false,
  firstNameValiated = false,
  lastNameHasDanger = false,
  lastNameValiated = false,
  passwordHasDanger = false,
  passwordValiated = false,
  isLoading = false,
}) => {
  const [mailAdress, setMailAdress] = useState("");
  const [userName, setUserName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");

  const handleChange = (
    event: ChangeInputValueEvent,
    setterFunc: (input: string) => void
  ) => {
    setterFunc(event.target.value);
  };
  return (
    <div className={styles["formInner"]}>
      {!isLoading && (
        <div>
          <TextInput
            className={"form-group"}
            value={mailAdress}
            onChange={(event) => handleChange(event, setMailAdress)}
            hasDanger={mailAdressHasDanger}
            wasValiated={mailAdressValiated}
            label="Adresse mail"
            type={inputTypes.email}
          />
          <TextInput
            className={"form-group"}
            value={userName}
            onChange={(event) => handleChange(event, setUserName)}
            hasDanger={userNameHasDanger}
            wasValiated={userNameValiated}
            label="Nom d'utilisateur - Composé uniquement des lettres et des chiffres."
            type={inputTypes.text}
          />
          <TextInput
            className={"form-group"}
            value={firstName}
            onChange={(event) => handleChange(event, setFirstName)}
            hasDanger={firstNameHasDanger}
            wasValiated={firstNameValiated}
            label="Prénom"
            type={inputTypes.text}
          />
          <TextInput
            className={"form-group"}
            value={lastName}
            onChange={(event) => handleChange(event, setLastName)}
            hasDanger={lastNameHasDanger}
            wasValiated={lastNameValiated}
            label="Nom de famille"
            type={inputTypes.text}
          />
          <TextInput
            className={"form-group"}
            value={password}
            onChange={(event) => handleChange(event, setPassword)}
            hasDanger={passwordHasDanger}
            wasValiated={passwordValiated}
            label="Mot de passe"
            type={inputTypes.password}
          />
          <Button
            stylesType="danger"
            textContent="Valider"
            onClick={registerFunction}
          ></Button>
        </div>
      )}
      {isLoading && <IconSpinner textMessage="Changement..."></IconSpinner>}
    </div>
  );
};
