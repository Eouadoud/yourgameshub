import clsx from "clsx";
import type { FC } from "react";
import { ComponentProps } from "react";
import styles from "./Container.module.scss";

export type ContainerProps = ComponentProps<"div"> & {
  "data-testid"?: string;
};

export const Container: FC<ContainerProps> = ({ className, ...props }) => {
  return (
    <div
      {...props}
      className={clsx(className, styles.container)}
      data-testid={props["data-testid"] || "container"}
    />
  );
};
