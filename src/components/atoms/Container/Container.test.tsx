import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Container } from "./";

describe("NextImage", () => {
  test("Test renders correctly", () => {
    const { container } = render(<Container />);
    expect(container).toMatchSnapshot();
  });

  test("Test classname prop", () => {
    const className = "test_class";
    render(<Container className={className} />);
    expect(screen.getByTestId("container")).toHaveClass(className);
  });
});
