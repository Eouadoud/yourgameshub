import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Button } from "./Button";

describe("Button", () => {
  test("test simple render", () => {
    render(<Button stylesType="success" textContent="save" />);
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement).toHaveTextContent("save");
    expect(buttonElement).toHaveClass("btn-success");
  });

  test("test cals onClick function when clicked", async () => {
    const cancelFunc = jest.fn();
    render(
      <Button stylesType="danger" textContent="cancel" onClick={cancelFunc} />
    );
    const buttonElement = screen.getByRole("button");
    await userEvent.click(buttonElement);
    expect(cancelFunc).toHaveBeenCalledTimes(1);
  });

  test("test applies custom class", () => {
    render(
      <Button
        stylesType="success"
        textContent="save"
        customClass="custom-button"
      />
    );
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toHaveClass("custom-button");
  });
});
