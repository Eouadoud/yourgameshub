import clsx from "clsx";
import { FC } from "react";
import styles from "./Button.module.scss";

type buttonProps = {
  icon?: string;
  stylesType: string;
  textContent: string;
  customClass?: string;
  onClick?(): void;
};

export const Button: FC<buttonProps> = ({
  icon,
  stylesType,
  textContent,
  customClass,
  onClick,
}) => {
  const buttonClasses = `btn btn-${stylesType}`;

  return (
    <button
      type="button"
      onClick={onClick}
      className={clsx(buttonClasses, customClass, styles["button"])}
    >
      {icon && <i className={`${icon}`}></i>}
      {textContent}
    </button>
  );
};
