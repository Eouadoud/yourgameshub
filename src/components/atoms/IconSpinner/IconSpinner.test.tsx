import { act, render, screen } from "@testing-library/react";
import { IconSpinner } from "./IconSpinner";
import "@testing-library/jest-dom";
//import { act } from "react-dom/test-utils";
import "react-dom/test-utils";

jest.useFakeTimers();

describe("IconSpinner", () => {
  test("test default render", () => {
    render(<IconSpinner />);
    const iconElement = screen.getByRole("iconspinner");
    expect(iconElement).toBeInTheDocument();
    expect(iconElement).toHaveAttribute("width", "50");
    expect(iconElement).toHaveAttribute("height", "50");
  });

  test("test render with custom test and text message", () => {
    render(<IconSpinner textMessage="Loading..." size={30} />);
    const iconElement = screen.getByRole("iconspinner");
    expect(iconElement).toBeInTheDocument();
    expect(iconElement).toHaveAttribute("width", "30");
    expect(iconElement).toHaveAttribute("height", "30");
    const textElement = screen.getByText("Loading...");
    expect(textElement).toBeInTheDocument();
  });
  /* eslint-disable */
  // eslint desabled temporary : to fix this part
  test("test updates rotation every 15ms", async () => {
    await act(async () => render(<IconSpinner />));
    const iconElement = screen.getByRole("iconspinner");
    act(() => jest.advanceTimersByTime(15));
    expect(iconElement).toHaveStyle("transform: rotate(5deg)");
    act(() => jest.advanceTimersByTime(30));
    expect(iconElement).toHaveStyle("transform: rotate(15deg)");
  });
});
