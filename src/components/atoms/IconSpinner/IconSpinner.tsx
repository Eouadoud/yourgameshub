import { FC, useEffect, useState } from "react";
import HomeIncon from "@/icons/yourgamessvg.svg";

type IconSpinnerProps = {
  textMessage?: string;
  size?: number;
};

export const IconSpinner: FC<IconSpinnerProps> = ({
  textMessage,
  size = 50,
}) => {
  const [rotationDegree, setRotationDegree] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setRotationDegree((prevDegree) => prevDegree + 5);
    }, 15);

    return () => clearInterval(interval);
  }, []);
  return (
    <div className="d-flex flex-column align-items-center justify-content-center">
      <div className="row">
        <HomeIncon
          role="iconspinner"
          width={size}
          height={size}
          style={{ transform: `rotate(${rotationDegree}deg)` }}
        />
      </div>
      <div className="row">
        {textMessage && <span className="sr-only">{textMessage}</span>}
      </div>
    </div>
  );
};
