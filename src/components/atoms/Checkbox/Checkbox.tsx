import clsx from "clsx";
import { FC } from "react";
import { checkBoxStyle } from "@/types/CheckBox";

type checkboxProps = {
  labelText?: string;
  stylesType: checkBoxStyle;
  cutomStyle?: string;
  checked?: boolean;
  disabled?: boolean;
  onClick?(): void;
  testId?: string;
};

export const CheckBox: FC<checkboxProps> = ({
  labelText,
  stylesType,
  cutomStyle,
  checked = false,
  disabled = false,
  testId,
  onClick,
}) => {
  return (
    <div
      data-testid={testId}
      className={clsx("form-check", stylesType, cutomStyle)}
    >
      <input
        onClick={onClick}
        checked={checked}
        disabled={disabled}
        className="form-check-input"
        type="checkbox"
      />
      <label className="form-check-label">{labelText}</label>
    </div>
  );
};
