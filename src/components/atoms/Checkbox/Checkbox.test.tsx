import { render, screen } from "@testing-library/react";
import { CheckBox } from "./Checkbox";
import { checkBoxStyle } from "@/types/CheckBox";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

describe("CheckBox", () => {
  test("test simple render", () => {
    render(
      <CheckBox
        stylesType={checkBoxStyle.standatd}
        labelText="CheckBoxText content."
      />
    );
    const labelElement = screen.getByText("CheckBoxText content.");

    expect(labelElement).toBeInTheDocument();
  });

  test("test stylesType, disable and check", () => {
    render(
      <CheckBox
        labelText="CheckBoxText content."
        stylesType={checkBoxStyle.switch}
        testId="checkBoxInner"
        disabled={true}
        checked={true}
      />
    );
    const checkboxElement = screen.getByRole("checkbox");
    const inner = screen.getByTestId("checkBoxInner");
    expect(inner).toHaveClass("form-switch");
    expect(checkboxElement).toBeDisabled();
    expect(checkboxElement).toBeChecked();
  });

  test("test call onclick function", async () => {
    const onClick = jest.fn();
    render(
      <CheckBox
        stylesType={checkBoxStyle.standatd}
        labelText="test call onclick."
        onClick={onClick}
      />
    );
    const checkboxElement = screen.getByRole("checkbox");
    await userEvent.click(checkboxElement);
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
