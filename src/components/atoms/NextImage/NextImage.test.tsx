import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { NextImage, NextImageProps } from "./";

const imageProps: NextImageProps = {
  src: "/test.png",
  alt: "test image",
  layout: "fill",
  unoptimized: true,
};

describe("NextImage", () => {
  test("Test renders correctly", () => {
    const { container } = render(<NextImage {...imageProps} />);
    expect(container).toMatchSnapshot();
  });

  test("Test classname prop & alt prop", () => {
    const className = "test_class";
    render(<NextImage className={className} {...imageProps} />);
    expect(screen.getByTestId("next-image-container")).toHaveClass(className);
    expect(screen.getByRole("img")).toHaveAttribute("alt", imageProps.alt);
  });
});
