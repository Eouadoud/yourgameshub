import clsx from "clsx";
import Image, { ImageProps } from "next/image";
import type { FC } from "react";
import styles from "./NextImage.module.scss";

export type NextImageProps = ImageProps;

export const NextImage: FC<NextImageProps> = ({
  className,
  alt,
  src,
  ...props
}) => {
  return (
    <div
      className={clsx(className, styles["next-image"])}
      data-testid="next-image-container"
    >
      <Image {...props} alt={alt} src={src} unoptimized />
    </div>
  );
};
