import { spinnerType, textType } from "@/types/BSclasses";
import clsx from "clsx";
import { FC } from "react";

type spinnerProps = {
  spanText?: string;
  className?: string;
  styled?: textType;
  styleType?: spinnerType;
};

export const Spinner: FC<spinnerProps> = ({
  spanText,
  className,
  styled = textType.dark,
  styleType = spinnerType.grow,
}) => {
  return (
    <div className="d-flex flex-column align-items-center justify-content-center">
      <div
        data-testId="spinner-zone"
        className={clsx("row", styleType, styled, className)}
        role="status"
      ></div>
      <div className="row">
        {spanText && <span className="sr-only">{spanText}</span>}
      </div>
    </div>
  );
};
