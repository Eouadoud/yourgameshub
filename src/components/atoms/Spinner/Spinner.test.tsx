import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Spinner } from "./Spinner";
import { spinnerType, textType } from "@/types/BSclasses";

describe("Spinner", () => {
  test("test default render", () => {
    render(<Spinner />);
    const spinnerElement = screen.getByRole("status");
    expect(spinnerElement).toBeInTheDocument();
    expect(spinnerElement).toHaveClass("spinner-grow");
    expect(spinnerElement).toHaveClass("text-dark");
  });

  test("test render with border spnner & primary style", () => {
    render(
      <Spinner
        spanText="Loading..."
        className="custom-spinner"
        styled={textType.primary}
        styleType={spinnerType.border}
      />
    );
    const spinnerElement = screen.getByRole("status");
    const spanElement = screen.getByText("Loading...");
    expect(spinnerElement).toBeInTheDocument();
    expect(spinnerElement).toHaveClass("spinner-border");
    expect(spinnerElement).toHaveClass("text-primary");
    expect(spinnerElement).toHaveClass("custom-spinner");
    expect(spanElement).toBeInTheDocument();
  });
});
