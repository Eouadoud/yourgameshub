import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Select } from "./Select";
import { selectOptions } from "@/Data/atoms";

describe("Select", () => {
  test("test default render", () => {
    const defaultOption = "Select an option";
    render(<Select options={selectOptions} defaultOption={defaultOption} />);
    const defaultOptionElement = screen.getByText(defaultOption);
    expect(defaultOptionElement).toBeInTheDocument();
    const selectElement = screen.getByRole("selectList");
    expect(selectElement).toBeInTheDocument();

    selectOptions.forEach((option) => {
      const optionElement = screen.getByText(option.content);
      expect(optionElement).toBeInTheDocument();
    });
  });

  test("selects multiple options", async () => {
    render(<Select options={selectOptions} multiple={true} />);
    const selectElement = screen.getByRole("selectList");

    await userEvent.selectOptions(selectElement, [
      selectOptions[0].value,
      selectOptions[2].value,
    ]);

    const selectedOptions = screen.getAllByRole("option", { selected: true });
    expect(selectedOptions).toHaveLength(2);
  });
});
