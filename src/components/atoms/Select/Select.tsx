import { selectOption } from "@/types/Select";
import clsx from "clsx";
import { FC } from "react";

type selectProps = {
  options?: selectOption[];
  className?: string;
  defaultOption?: string;
  multiple?: boolean;
};

export const Select: FC<selectProps> = ({
  options,
  className,
  defaultOption,
  multiple = false,
}) => {
  return (
    <select
      role="selectList"
      className={clsx(className, "form-select")}
      multiple={multiple}
    >
      {defaultOption && <option>{defaultOption}</option>}
      {options?.map((e, index) => (
        <option key={index} value={e.value}>
          {e.content}
        </option>
      ))}
    </select>
  );
};
