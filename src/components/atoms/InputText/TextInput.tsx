import clsx from "clsx";
import { FC, useMemo, ComponentProps } from "react";
import styles from "./TextInput.module.scss";
import { inputLabelFeedBackClass } from "@/types/InputValidation";
import { ChangeInputValueEvent } from "@/types/Input";

type inputProps = Omit<
  ComponentProps<"input">,
  "onChange" | "onBlur" | "name"
> & {
  label?: string;
  value?: string;
  className?: string;
  labelClassName?: string;
  feedBackMessage?: string;
  onChange?: (event: ChangeInputValueEvent) => void;
  wasValiated: boolean;
  hasDanger: boolean;
};

export const TextInput: FC<inputProps> = ({
  label,
  labelClassName,
  className,
  value,
  onChange,
  feedBackMessage,
  wasValiated = false,
  hasDanger = false,
  ...props
}) => {
  const showFeedBack = useMemo(
    () =>
      feedBackMessage && (hasDanger || wasValiated) && hasDanger != wasValiated,
    [feedBackMessage, hasDanger, wasValiated]
  );

  return (
    <div
      className={clsx(
        styles["input-inner"],
        className,
        wasValiated && !hasDanger ? "was-validated" : ""
      )}
    >
      <label className={clsx("form-label", labelClassName)}>{label}</label>
      <input
        {...props}
        className={clsx(
          "form-control",
          hasDanger && !wasValiated ? styles["invalid-input"] : ""
        )}
        value={value}
        onChange={onChange}
      />
      {showFeedBack && (
        <label
          className={clsx(
            hasDanger && !wasValiated ? inputLabelFeedBackClass.invalid : "",
            wasValiated && !hasDanger ? inputLabelFeedBackClass.valid : "",
            styles["feed-back-label"]
          )}
        >
          {feedBackMessage}
        </label>
      )}
    </div>
  );
};
