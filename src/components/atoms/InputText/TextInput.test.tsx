import { render, screen, fireEvent } from "@testing-library/react";
import { TextInput } from "./TextInput";
import "@testing-library/jest-dom";

describe("TextInput", () => {
  test("test simple render", () => {
    render(
      <TextInput
        label="Name:"
        value="testVal"
        className="custom-input"
        labelClassName="custom-label"
        wasValiated={true}
        hasDanger={false}
        feedBackMessage="Invalid input"
      />
    );
    const labelElement = screen.getByText("Name:");
    const inputElement = screen.getByDisplayValue("testVal");
    const feedbackElement = screen.getByText("Invalid input");
    expect(labelElement).toBeInTheDocument();
    expect(inputElement).toBeInTheDocument();
    expect(feedbackElement).toBeInTheDocument();
    expect(labelElement).toHaveClass("custom-label");
    expect(feedbackElement).toHaveClass("valid-feedback");
  });

  test("test calls onChange function", () => {
    const changeFunc = jest.fn();
    render(
      <TextInput wasValiated={true} hasDanger={false} onChange={changeFunc} />
    );
    const inputElement = screen.getByRole("textbox");
    fireEvent.change(inputElement, { target: { value: "New Value" } });
    expect(changeFunc).toHaveBeenCalledTimes(1);
  });
});
