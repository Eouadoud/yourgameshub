import { ChangeEventHandler } from "react";

export type ChangeInputValueEvent = Parameters<
  ChangeEventHandler<HTMLInputElement>
>[0];

export enum inputTypes {
  date = "date",
  email = "email",
  number = "number",
  password = "password",
  time = "time",
  url = "url",
  text = "text",
}
