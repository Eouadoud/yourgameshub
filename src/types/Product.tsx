export type productForCard = {
  id: string;
  title: string;
  descrition: Text;
  price: number;
  inWishList?: boolean;
};
