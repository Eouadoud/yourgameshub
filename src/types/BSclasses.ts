export enum textType {
  primary = "text-primary",
  secondary = "text-secondary",
  success = "text-success",
  danger = "text-danger",
  warning = "text-warning",
  info = "text-info",
  light = "text-light",
  dark = "text-dark",
}

export enum spinnerType {
  border = "spinner-border",
  grow = "spinner-grow",
}
