import { selectOption } from "@/types/Select";

export const selectOptions: selectOption[] = [
  {
    content: "One",
    value: "1",
  },
  {
    content: "Two",
    value: "2",
  },
  {
    content: "Three",
    value: "3",
  },
];
