const config = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["node_modules", "dist"],

  testEnvironment: "jest-environment-jsdom",
  testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/dist/"],
  modulePathIgnorePatterns: ["<rootDir>/dist/"],
  transformIgnorePatterns: ["node_modules/(?!@swiper)/"],
  moduleNameMapper: {
    "\\.(css|scss|sass)$": "identity-obj-proxy",
    "\\.svg$": "<rootDir>/__mocks__/svg.tsx",
    "swiper/react": "<rootDir>/node_modules/swiper/react/swiper-react.js",
    "next/image": "<rootDir>/__mocks__/image.tsx",
  },
};

export default config;
