const path = require("path");
const pathToInlineSvg = path.resolve(__dirname, "../src/icons/");

module.exports = {
  stories: ["../src/**/*.stories.@(tsx|jsx|mdx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-a11y",
    "@storybook/addon-postcss",
  ],
  webpackFinal: async (config) => {
    const fileLoaderRule = config.module.rules.find(
      (rule) => rule.test && rule.test.test(".svg")
    );
    fileLoaderRule.exclude = pathToInlineSvg;

    config.module.rules.push({
      test: /\.svg$/,
      include: pathToInlineSvg,
      use: [
        {
          loader: "@svgr/webpack",
          options: {
            icon: true,
          },
        },
      ],
    });

    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "resolve-url-loader", "sass-loader"],
      include: path.resolve(__dirname, "../"),
    });

    config.resolve.alias["@"] = path.resolve(__dirname, "../src/");
    return config;
  },
  staticDirs: ["../public"],
  typescript: {
    check: true, // type-check stories during Storybook build
  },
  core: {
    builder: "webpack5",
  },
  framework: "@storybook/react",
};
