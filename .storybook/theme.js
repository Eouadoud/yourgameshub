import { create } from "@storybook/theming";

export default create({
  base: "light",
  brandTitle: "yourgameshub",
  brandImage: "https://iili.io/HLiuLkF.png",

  appBg: "whitesmoke",
  colorPrimary: "#2C5EA0",
  colorSecondary: "#706262",
});
