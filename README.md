YOURGAMESHUB project
a front-end project built using React, TypeScript, and following the Atomic Design pattern.
....
Features
Browse through a wide range of video games across various types.
Search for games using keywords, filters, and sorting options.
....
Installation
for install : yarn 
start development storybook server: yarn storybook
Open http://localhost:6006 in your browser.

License
This project is licensed under the MIT License.

Feel free to customize and expand upon this description according to your specific project requirements.

-------------------
Atoms: 
    DONE: 
        - Bouton.
        - Input.
        - Checkbox.
        - Image.
        - Container
        - Spinner.
        - Select.
    TODO:
        - Icon.
        - Badge.
        - Avatar
        - Tooltip.
        - ProgressBar.
        - Divider.

Mollucules: 
    - SearchBar
    - ProductCard
    - FilterBar
    - LoginForm
    - RegisterForm.
 