/* eslint-disable @next/next/no-img-element */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ImageProps } from "next/image";
import React, { FC } from "react";

const NextImageMock: FC<ImageProps> = ({
  alt,
  src,
  unoptimized,
  priority,
  ...props
}) => <img src={src as string} alt={alt} {...props} />;

export { ImageProps };

export default NextImageMock;
