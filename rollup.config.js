import alias from "@rollup/plugin-alias";
import commonjs from "@rollup/plugin-commonjs";
import resolve, {
  DEFAULTS as RESOLVE_DEFAULTS,
} from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import svgr from "@svgr/rollup";
import path from "path";
import dts from "rollup-plugin-dts";
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";
import { terser } from "rollup-plugin-terser";
import packageJson from "./package.json";

const pathToSrc = path.resolve(__dirname, "./src/");

const config = [
  {
    input: "src/index.ts",
    output: [
      {
        file: packageJson.main,
        format: "cjs",
        sourcemap: true,
        inlineDynamicImports: true,
      },
      {
        file: packageJson.module,
        format: "esm",
        sourcemap: true,
        inlineDynamicImports: true,
      },
    ],
    plugins: [
      peerDepsExternal(),
      resolve({
        ...RESOLVE_DEFAULTS,
        mainFields: ["module", "main"].filter(Boolean),
        extensions: [
          ...RESOLVE_DEFAULTS.extensions,
          ".cjs",
          ".mjs",
          ".jsx",
          ".tsx",
          ".css",
          ".sass",
          ".scss",
          ".svg",
        ],
      }),
      commonjs({
        include: ["node_modules/**"],
      }),
      typescript({
        tsconfig: "./tsconfig.json",
        exclude: [
          "dist",
          "node_modules",
          "src/**/*.test.tsx",
          "src/**/*.stories.tsx",
        ],
      }),
      svgr({
        icon: true,
      }),
      postcss({
        modules: {
          generateScopedName: "[name]__[local]___[hash:base64:5]",
        },
      }),
      alias({
        entries: [{ find: "@", replacement: pathToSrc }],
      }),
      terser(),
    ],
    external: ["react", "react-dom", "next/dynamic"],
  },
  {
    input: "dist/esm/index.d.ts",
    output: [{ file: "dist/index.d.ts", format: "esm" }],
    plugins: [dts()],
  },
];

export default config;
